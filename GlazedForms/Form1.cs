﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GlazedForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBoxMaterial.SelectedIndex = 0;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            double width = Convert.ToDouble(textBoxWidth.Text);
            double height = Convert.ToDouble(textBoxHeight.Text);

            double size = width * height;

            double cost = 0;
            if (comboBoxMaterial.SelectedIndex == 0 && radioButton1.Checked) cost = size * 0.25;
            if (comboBoxMaterial.SelectedIndex == 0 && radioButton2.Checked) cost = size * 0.30;
            if (comboBoxMaterial.SelectedIndex == 1 && radioButton1.Checked) cost = size * 0.05;
            if (comboBoxMaterial.SelectedIndex == 1 && radioButton2.Checked) cost = size * 0.10;
            if (comboBoxMaterial.SelectedIndex == 2 && radioButton1.Checked) cost = size * 0.15;
            if (comboBoxMaterial.SelectedIndex == 2 && radioButton2.Checked) cost = size * 0.20;
            if (checkBox1.Checked) cost += 35;
            label5.Text = "Вартiсть: " + cost + " грн.";
        }

        private void Label7_Click(object sender, EventArgs e)
        {
         
        }

        private void Label5_Click(object sender, EventArgs e)
        {
            label5.Text = "Поставте 5 будь ласка";
        }
    }
}
